###enrichment for 1 set

###input
#gene_list (list of genes in ID format (e.g. WBID))
#gene_total (all genes detected with platform (e.g. Microarray))
#anno_group (annotation-group to be enriched for)

###output
#dataframe with enrichment,  columns: Group, Genes_in_group, Overlap_expected, Overlap, Significance 

###Description
# Basic enrichment function, normally not used directly.

enrich.one <- function(gene_list,gene_total,anno_group){
                        if(missing(gene_list)){                                stop("genes, ID format")}
                        if(missing(gene_total)){                               stop("total set of genes (e.g. present on MA), ID format")}
                        if(missing(anno_group)){                               stop("Give an annotation-group file")}

                        ###make absolutely sure that we work with unique genes
                        anno_group[[1]] <- anno_group[[1]][!duplicated(paste(as.character(unlist(anno_group[[1]][,1])),as.character(unlist(anno_group[[1]][,2])))),]
                        anno_group[[1]][,2] <- as.factor(anno_group[[1]][,2])
                        gene_list <- unique(gene_list)
                        gene_total <- unique(gene_total)

                        wb.drawn <- table(anno_group[[1]][anno_group[[1]][,1] %in% gene_list,2])
                        tot.wb <- table(anno_group[[1]][anno_group[[1]][,1] %in% gene_total,2])
                        tot.bb <- length(unique(gene_total))- tot.wb
                        tot.drawn <- length(gene_list)
                        enr.test <- phyper(wb.drawn,tot.wb,tot.bb,tot.drawn,lower.tail=F)

                        output <- data.frame(rownames(tot.wb),as.numeric(tot.wb),as.numeric(tot.wb)*(sum(gene_list %in% gene_total)/length(gene_total)),as.numeric(wb.drawn),as.numeric(enr.test))
                        colnames(output) <- c("Group","Genes_in_group","Overlap_expected","Overlap","Significance")

                        return (output)
}
