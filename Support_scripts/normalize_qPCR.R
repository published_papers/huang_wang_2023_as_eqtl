###   Load the qPCR data   #########################################################################      

    #Load target file
      target.file <- read.delim("H:/Projects/PD RILs/Gene_expression_asyn/qPCR_results/Target_qPCR_a-syn_YFP.txt", header = TRUE); head(target.file)
      
    #With the file names in the target file, you can load the data
      data.file <- TargetLoading(target.file, "H:/Projects/PD RILs/Gene_expression_asyn/qPCR_results/Raw")
      
    #Specificy names of RILs with a crappy measurement and parents
      crap.RILs <- filter(target.file, Data.status == "Crap")$Strain
      parents <- c("NL5901", "N2", "SCH4856", "CB4856")
        
    #Set primer names
      primer.names <- c("a-syn","a-syn","a-syn","a-syn","Y37E3.7","Y37E3.7","RPL-6","RPL-6")
      ref.primers <- c("Y37E3.7","RPL-6")
      
    #The pre-processing function calculates mean, sd and n per primer over the technical replicates
    #It also transforms the data to log2(2^(x-Ct))
      
      data.prePro <- PreProcess(targets=target.file, data.input=data.file, primer.names=primer.names)
      data.prePro <- filter(data.prePro, Data.status != "Crap")
      target.file <- filter(target.file, Data.status != "Crap")
      
    #The normalization function normalises the data using the given reference genes
      data.norm <- NormData(targets=target.file, PreProcess.file=data.prePro, ref.primers=ref.primers); head(data.norm)

    #Select data for plotting
      qpcr_data <- data.norm
      
      save(qpcr_data,file="obj_qpcr_data.Rdata")
      