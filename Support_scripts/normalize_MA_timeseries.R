################################################################################
###normalization of expression data
################################################################################


    rg.norm <- transcriptomics.agilent.norm(targets = targets.RIL,
                                  array_dir = "W:/PROJECTS/NEMmasstorage/C.elegans/Data/MA/MA_elegans/", 
                                  save_dir = paste(workwd,"/Normalized/",sep=""),
                                  filename = filename)
    
    trans.int <- transcriptomics.transform.norm(rg.norm,filename=filename,save_dir=paste(workwd,"/Data/MA/",sep=""))

    ###Checks
    correlsums <- transcriptomics.check.cor(trans.int,filename=filename,save_dir=paste(workwd,"/Data/MA/",sep=""))
    transcriptomics.check.genes(trans.int,spot.id=agi.id$gene_public_name,filename=filename,save_dir=paste(workwd,"/Data/MA/",sep=""))

    ###Make a list
    colnames.sep <- ":"
    colnames.names <- c("number","strain","batch","alphasyn","days","sample_number")

    list.data <- transcriptomics.list.to.df(trans.int = trans.int, spot.id=agi.id$SpotID,colnames.sep = colnames.sep, colnames.names = colnames.names)
    save(list.data,file=paste(workwd,"/Normalized/obj_list.data.Rdata",sep=""))
    
    
